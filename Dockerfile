FROM node:16-buster-slim

MAINTAINER Martin Alker <martin.alker@mailbox.org>

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y \
        autoconf \
        build-essential && \
    apt-get clean autoclean && \
    apt-get autoremove --yes && \
    rm -rf /var/lib/{apt,dpkg,cache,log}/

CMD ["/bin/bash"]
